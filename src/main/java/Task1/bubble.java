package Task1;

public class bubble {
    public static void main(String[] args) {
        int count = 0;
        int[] array = new int[] {1,7,3,8,5};
        for (int i =1; i < array.length; i++ ) {
            for (int j = 0; j < array.length - 1; j++){
                count++;
                if (array[j] > array[j+1]){
                    int temp = array[j];
                    array[j] = array[j+1];
                    array[j + 1] = temp;
                }
            }
        }
        for (int c : array){
            System.out.print(c + " ");
        }
        System.out.println("Count " + count);
    }
}
